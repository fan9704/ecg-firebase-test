// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    // ignore: missing_enum_constant_in_switch
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
    }

    throw UnsupportedError(
      'DefaultFirebaseOptions are not supported for this platform.',
    );
  }

  static const FirebaseOptions web = FirebaseOptions(
      apiKey: "AIzaSyBUHg7bz4jJ8GK3MX73ybYaPptAEV6Nc2g",
      appId: "1:309399535230:web:3487c074e49c5d0fb8222f",
      messagingSenderId: "309399535230",
      projectId: "ecg-backend",
      authDomain: "ecg-backend.firebaseapp.com",
      storageBucket: "ecg-backend.appspot.com",
      measurementId: "G-JMQ0B411JM"
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyAbnJBhrby-qosjk2obn7Dx42n73JdJcDI',
    appId: '1:309399535230:android:8e611a6e7b8b692db8222f',
    messagingSenderId: '309399535230',
    projectId: 'ecg-backend',
    storageBucket: 'ecg-backend.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyCcC2JxlpKgjFkFK2OjbWaXt4u3P2X2AkU',
    appId: '1:309399535230:ios:107c8a620fc58bbdb8222f',
    messagingSenderId: '309399535230',
    projectId: 'ecg-backend',
    storageBucket: 'ecg-backend.appspot.com',
    iosClientId: '309399535230-qptmq9hi82sd7c6u33isp3fsr781h09q.apps.googleusercontent.com',
    iosBundleId: 'ecg',
  );
}
